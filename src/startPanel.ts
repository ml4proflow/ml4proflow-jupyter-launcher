import {
    ILabShell,
    JupyterFrontEnd
  } from '@jupyterlab/application';

import { ILauncher } from '@jupyterlab/launcher';

import { ICommandPalette, MainAreaWidget } from '@jupyterlab/apputils';
import { MyLauncherModel, MyLauncher } from './launcher';
import { ITranslator } from '@jupyterlab/translation';
import { launcherIcon } from '@jupyterlab/ui-components';

import { toArray } from '@lumino/algorithm';
import { ReadonlyPartialJSONObject } from '@lumino/coreutils';
import { Widget } from '@lumino/widgets';

import { CommandIDs } from './index';



/**
* Activate the launcher.
*/
export function startPanel(
    app: JupyterFrontEnd,
    translator: ITranslator,
    labShell: ILabShell | null,
    palette: ICommandPalette | null
): ILauncher {
    console.log('This extension generates a new Start Panel instead of the default launcher!');


    const { commands, shell } = app;
    const trans = translator.load('jupyterlab');
    const model = new MyLauncherModel();

    commands.addCommand(CommandIDs.create, {
        label: trans.__('New Launcher'),
        execute: (args: ReadonlyPartialJSONObject) => {
            const cwd = args['cwd'] ? String(args['cwd']) : '';
            const id = `launcher-${Private.id++}`;
            const callback = (item: Widget) => {
                shell.add(item, 'main', { ref: id });
            };
            const launcher = new MyLauncher({
                model,
                cwd,
                callback,
                commands,
                translator
            });

            launcher.model = model;
            launcher.title.icon = launcherIcon;
            launcher.title.label = trans.__('ml4proflow');

            const main = new MainAreaWidget({ content: launcher });

            // If there are any other widgets open, remove the launcher close icon.
            main.title.closable = !!toArray(shell.widgets('main')).length;
            main.id = id;

            shell.add(main, 'main', { activate: args['activate'] as boolean });

            if (labShell) {
                labShell.layoutModified.connect(() => {
                    // If there is only a launcher open, remove the close icon.
                    main.title.closable = toArray(labShell.widgets('main')).length > 1;
                }, main);
            }

            return main;
        }
    });

    if (palette) {
        palette.addItem({
            command: CommandIDs.create,
            category: trans.__('Launcher')
        });
    }

    return model;
}

/**
 * The namespace for module private data.
 */
 namespace Private {
    /**
     * The incrementing id used for launcher widgets.
     */
    export let id = 0;
  }