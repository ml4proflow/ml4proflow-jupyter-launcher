import { JupyterFrontEnd } from '@jupyterlab/application'
import { ILauncher } from '@jupyterlab/launcher';

import { 
  //settingsIcon,
  notebookIcon,
  runIcon
  //codeIcon
} from '@jupyterlab/ui-components';
import { 
  CommandIDs,
  label4mods,
  path4mods,
  label4gui,
  path4gui 
} from './index';


/**
 * Shortcut Function 
 */
export function shortcut1(app: JupyterFrontEnd, launcher: ILauncher) {
    console.log('This extension enables you to add custom shortcuts to the launcher!');

    // Notebooks
    for (let i in path4mods) {
        const path = path4mods[i];
        app.commands.addCommand(CommandIDs.mod_CommandIDs[i], {
            label: label4mods[i],
            icon: notebookIcon,
            execute: () => {
                app.commands.execute('docmanager:open', {
                    path: path,
                });
            }
        });
        if (launcher) {
            launcher.add({
                command: CommandIDs.mod_CommandIDs[i],
                category: 'Modules'
            });
        }
    }

    for (let i in path4gui) {
        const path = path4gui[i];
        app.commands.addCommand(CommandIDs.gui_CommandIDs[i], {
            label: label4gui[i],
            icon: runIcon,
            execute: () => {
                app.commands.execute('docmanager:open', {
                    path: path,
                });
            }
        });
        if (launcher) {
            launcher.add({
                command: CommandIDs.gui_CommandIDs[i],
                category: 'Graphical User Interface'
            });
        }
    }
}
