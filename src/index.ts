import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
  ILabShell
} from '@jupyterlab/application';
import { ILauncher } from '@jupyterlab/launcher';
import { ICommandPalette } from '@jupyterlab/apputils';
import { ITranslator } from '@jupyterlab/translation';
//import { PathExt } from '@jupyterlab/coreutils';

import { requestAPI } from './handler';

import { startPanel } from './startPanel';
import { shortcut1} from './shortcut';

/**
 * Initialization of variables
 */
//let nb_commands = new Array<string>(files.length);
//let nb_path = new Array<string>(files.length);
//export let files: Array<string>;
//export let nb_commands: Array<string>;
//export let nb_path: Array<string>;
let mod_files: string[] = new Array();
let mod_path: string[] = new Array();
let mod_commands: string[] = new Array();
let mod_label: string[] = new Array();

let gui_files: string[] = new Array();
let gui_path: string[] = new Array();
let gui_commands: string[] = new Array();
let gui_label: string[] = new Array();


/**
 * Initialization data for the ml4proflow-jupyter-launcher extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'ml4proflow-jupyter-launcher:plugin',
  autoStart: true,
  activate: (app: JupyterFrontEnd) => {
    console.log('JupyterLab extension ml4proflow-jupyter-launcher is activated!');

    requestAPI<any>('get_example')
      .then(data => {
        mod_files = data['mod_files'];
        for (let mod_index in mod_files) {
          console.log(mod_files[mod_index]); /**Funktioniert -> Einzelne FileNames werden geprinted -> EVTL. "/" und "\" vereinheitlichen*/
          mod_path[mod_index] = 'notebooks/' + mod_files[mod_index] + '.ipynb';
          mod_commands[mod_index] = mod_files[mod_index] + ':open';
          mod_label[mod_index] = mod_files[mod_index] + '';
        }

        gui_files = data['gui_files'];
        for (let gui_index in gui_files) {
          console.log(gui_files[gui_index]); /**Funktioniert -> Einzelne FileNames werden geprinted -> EVTL. "/" und "\" vereinheitlichen*/
          gui_path[gui_index] = 'gui/' + gui_files[gui_index] + '.ipynb';
          gui_commands[gui_index] = gui_files[gui_index] + ':open';
          gui_label[gui_index] = gui_files[gui_index] + '';
        }
      })
      .catch(reason => {
        console.error(
          `The ml4proflow-jupyter-launcher server extension appears to be missing.\n${reason}`
        );
      });
  }
};

const launcher: JupyterFrontEndPlugin<ILauncher> = {
  activate: startPanel,
  id: 'toolbox:launcher',
  requires: [ITranslator],
  optional: [ILabShell, ICommandPalette],
  provides: ILauncher,
  autoStart: true
};

const shortcut: JupyterFrontEndPlugin<void> = {
  id: 'toolbox:shortcut',
  autoStart: true,
  requires: [ILauncher],
  activate: shortcut1
};

/**
 * Exports
 */
export namespace CommandIDs {
  export const create: string = 'launcher:create';
  export const mod_CommandIDs: Array<string> = mod_commands;
  export const gui_CommandIDs: Array<string> = gui_commands;
}

export let label4mods: string[] = mod_label;
export let path4mods: string[] = mod_path;
export let label4gui: string[] = gui_label;
export let path4gui: string[] = gui_path;

export default [plugin, shortcut, launcher];
