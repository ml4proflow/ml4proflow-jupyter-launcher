import json
import sys
from pathlib import Path
import glob

from jupyter_server.base.handlers import APIHandler
from jupyter_server.utils import url_path_join
import tornado

class RouteHandler(APIHandler):
    # The following decorator should be present on all verb methods (head, get, post,
    # patch, put, delete, options) to ensure only authorized user can request the
    # Jupyter server
    @tornado.web.authenticated
    def get(self):
        env_path = sys.exec_prefix
        nb_path = env_path + '/ml4proflow/notebooks/*.ipynb'
        gui_path = env_path + '/ml4proflow/gui/*.ipynb'
        nb_list = glob.glob(nb_path)
        gui_list = glob.glob(gui_path)
        nb_names: list = []
        gui_names: list = []

        for notebook in nb_list:
            nb_names.append(Path(notebook).stem)

        for gui in gui_list:
            gui_names.append(Path(gui).stem)

        self.finish(json.dumps({
           "mod_files": nb_names, "gui_files": gui_names
        }))


def setup_handlers(web_app):
    host_pattern = ".*$"

    base_url = web_app.settings["base_url"]
    route_pattern = url_path_join(base_url, "ml4proflow-jupyter-launcher", "get_example")
    handlers = [(route_pattern, RouteHandler)]
    web_app.add_handlers(host_pattern, handlers)
